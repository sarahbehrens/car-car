from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from .encoders import SalesRecordListEncoder, SalesPersonDetailEncoder, CustomerDetailEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord
from django.http import JsonResponse
import json


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 422
            return response


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 422
            return response
        



@require_http_methods(["GET", "POST"])
def api_list_sales_records(request, employee_number=None):
    if request.method == "GET":
        if employee_number is None:
            sales = SalesRecord.objects.all()
        else:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            sales = SalesRecord.objects.filter(sales_person=sales_person)
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordListEncoder
        )
    else:
        content = json.loads(request.body)
        
        automobile_vin = content["automobile"]
        vin = AutomobileVO.objects.get(vin=automobile_vin)
        content["automobile"] = vin

        employee_number = content["sales_person"]
        sales_person = SalesPerson.objects.get(employee_number=employee_number)
        content["sales_person"] = sales_person

        customer_id = content["customer"]
        customer = Customer.objects.get(id=customer_id)
        content["customer"] = customer
        try:
            vin = AutomobileVO.objects.get(vin=automobile_vin)
            if SalesRecord.objects.get(automobile=vin):
                return JsonResponse(
                {"message": "Automobile already sold"},
                status=404
            )
        except:
            sale = SalesRecord.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesRecordListEncoder,
                safe=False,
            )








        
        