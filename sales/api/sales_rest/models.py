from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)


class SalesRecord(models.Model):
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name='+',
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name='+',
        on_delete=models.PROTECT
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.CASCADE
    )



