# CarCar

Team:

* Person 1 - Sarah
* Person 2 - Jamie

## Design

## Service microservice

For the service API, I created 3 models: 
	- Technician with the name and employee_number, 
	
    - AutomobileVO to be updated with the poller file (the poller file pings the inventory API for the vin number), and
	
    - Appointment with the vin number of the owner's car, owner_name, appointment date/time, technician as a foreign key, reason for the    appointment, and 2 boolean fields. 
        The service_complete boolean field had a default of false since upon creation of an appointment it won't be complete. I added a function on the model to update the field to True when the appointment/complete/ url gets pinged (the url is pinged when someone hits the Finished button on the /api/services list page). 
        The second boolean field, "VIP_treatment", was updated to True or False in the POST view based on whether the VIN number provided in the body was in the automobileVO queryset: True if the VIN number was in the queryset, else False.


## Sales microservice

For this project I created four models for the sales microservice.  A SalesPerson model was designed to generate a sales person and to reference within other models.  Customer model is to created a customer and also used as a foreign key to another.  SalesRecord model was formed to take in three foreign keys and also price.  In order to get the data to create a sales record the customer and sales person information was required.  Also another foreign key to the AutomobileVO was essential.  

The AutomobileVO is what receives data from the poller that collects data from the Inventory microservice.  Data from the automobile is used to determine what vehicles are available and able to be used to create a sales record.  If the automobile vin was already used in a sales record, then it would not show up in the dropdown of automobile vins available.  