from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveSmallIntegerField(unique=True)

class Appointment(models.Model):
    owner_name = models.CharField(max_length=50)
    appointment_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name='+',
        on_delete=models.PROTECT,
    )
    service_complete = models.BooleanField(default=False)
    automobile_vin = models.CharField(max_length=17)
    VIP_treatment = models.BooleanField()

    def complete(self):
        self.service_complete = True
        self.save()