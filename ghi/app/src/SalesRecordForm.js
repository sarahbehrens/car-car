import React from 'react';



class SalesRecordForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            automobiles: [],
            sales_persons: [],
            customers: [],
            price: '',
            vins: [],
            createdSalesRecord: false
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    handleInputChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }



    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.automobiles
        delete data.customers
        delete data.sales_persons
        delete data.vins
        delete data.createdSalesRecord

        const SalesRecordUrl = 'http://localhost:8090/api/sales_records/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(SalesRecordUrl, fetchConfig);
        if (response.ok) {
            // eslint-disable-next-line
            const newSalesRecord = await response.json();
            const cleared = {
                automobile: '',
                sales_person: '',
                customer: '',
                price: '',
                createdSalesRecord: true
            };
            this.setState(cleared);
        }
    };
    



    async componentDidMount() {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
        const customerUrl = 'http://localhost:8090/api/customer/';
        const salesRecordUrl = 'http://localhost:8090/api/sales_records/';

        const automobileResponse = await fetch(automobileUrl)
        const salesPersonResponse = await fetch(salesPersonUrl)
        const customerResponse = await fetch(customerUrl)
        const salesRecordResponse = await fetch(salesRecordUrl)

        if (salesRecordResponse.ok) {
            const salesRecordData = await salesRecordResponse.json()
            const vins = []
            for (let sale of salesRecordData.sales) {
                let vin = sale.automobile
                vins.push(vin)
            }
            this.setState({vins: vins})
        }
    
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json()
            const availableInventory = []
            for (const auto of automobileData.autos) {
                if (!(this.state.vins.includes(auto.vin))) {
                    availableInventory.push(auto)
                }
            }
            this.setState({automobiles: availableInventory})
        };
        if (salesPersonResponse.ok) {
            const salesPersonData = await salesPersonResponse.json()
            this.setState({sales_persons: salesPersonData.sales_persons})
        }
        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            this.setState({customers: customerData.customers})
        }
    }
    
    render() {

        let messageClasses = 'alert alert-success p-2 mt-5 d-none mb-0';
        if (this.state.createdSalesRecord) {
            messageClasses = 'alert alert-success mb-0';
        }
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="text-center shadow p-4 mt-4">
                <h1>Create a new sales record</h1>
                <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                <div className="mb-3">
                    <select onChange={this.handleInputChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                    <option value="">Choose an automobile</option>
                    {this.state.automobiles.map(automobile => {
                            return (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleInputChange} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                    <option value="">Choose an sales person</option>
                    {this.state.sales_persons?.map(sales_person => {
                            return (
                                <option key={sales_person.employee_number} value={sales_person.employee_number}>
                                    {sales_person.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleInputChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                    <option value="">Choose an customer</option>
                    {this.state.customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <input onChange={this.handleInputChange} value={this.state.price} placeholder="Price" required type="number" name="price" id="price" className="form-control currency" data-symbol="$"/>
                    <label htmlFor="price"></label>
                </div>
                <button className="btn btn-success">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    A new sales record has been created. Congrats!
                </div>
            </div>
            </div>
        </div>
        );
    }
}

export default SalesRecordForm;