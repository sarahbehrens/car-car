import React from "react";


class ModelsList extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            models: []
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            this.setState({models: data.models})
        }
    }

    render() {
        return (
            <div>
                <h1 className="text-center mt-5 mb-5">Vehicle Models</h1>
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.models.map(model => {
                        return (
                            <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <img src={model.picture_url} className="img-thumbnail" alt="" width="200"  />
                            </td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
        )
    }
}

export default ModelsList;