import React from "react";


class CustomersList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customers: []
        };
    }

    async componentDidMount() {
        const customerUrl = 'http://localhost:8090/api/customer/'
        const response = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({customers: data.customers})
        }
    }
    
    render() {
        return (
            <div>
            <h1 className="text-center mt-4 mb-5">Employees</h1>
            <table className="table table-striped mt-4">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone number</th>
                </tr>
                </thead>
                <tbody>
                    {this.state.customers.map(customer => {
                        return (
                            <tr key={customer.id} >
                                <td>{customer.name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        );
    }
}

export default CustomersList;